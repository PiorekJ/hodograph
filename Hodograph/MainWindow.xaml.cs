﻿using System;
using System.Collections.ObjectModel;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media;
using Hodograph.Core;
using Hodograph.Utils;
using OpenTK.Graphics.OpenGL;
using OxyPlot;
using InputManager = Hodograph.Core.InputManager;
using Model = Hodograph.Core.Model;

namespace Hodograph
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public Simulation Simulation { get; set; }




        public MainWindow()
        {
            Simulation = new Simulation();
            InitializeComponent();
        }

        private void DisplayControl_OnControlLoaded(object sender, RoutedEventArgs e)
        {
            Simulation.InitializeSimulation();
            Simulation.Scene.Camera.SetViewportValues(DisplayControl.AspectRatio, DisplayControl.ControlWidth, DisplayControl.ControlHeight);
            Simulation.Storage.SetDisplayDimensions(DisplayControl.ControlWidth, DisplayControl.ControlHeight, DisplayControl.AspectRatio);
            GL.Viewport(0, 0, DisplayControl.ControlWidth, DisplayControl.ControlHeight);
            GL.Enable(EnableCap.VertexProgramPointSize);
            CompositionTarget.Rendering += Render;
        }

        private void Render(object sender, EventArgs e)
        {
            InputManager.SetMousePosition(DisplayControl.MousePosition);
            Simulation.TimeCounter.CountDT();


            GL.ClearColor(System.Drawing.Color.White);

            Simulation.Scene.Camera.UpdateCamera(DisplayControl.MousePosition);

            GL.BindFramebuffer(FramebufferTarget.Framebuffer, 0);
            GL.Clear(ClearBufferMask.ColorBufferBit | ClearBufferMask.DepthBufferBit);

            DrawScene();

            DisplayControl.SwapBuffers();
        }

        private float _leftDt = 0.0f;
        private void DrawScene()
        {
            Simulation.Scene.SceneBackground.Render();

            var delta = Simulation.DeltaTime;
            _leftDt += delta;
            while (_leftDt >= Constants.SimulationDT)
            {
                _leftDt -= Constants.SimulationDT;
                Simulation.Scene.ScenePiston.Update();
            }
            Simulation.Scene.ScenePiston.Render();

            for (int i = 0; i < Simulation.Scene.SceneObjects.Count; i++)
            {
                Simulation.Scene.SceneObjects[i].Render();
            }
        }

        private void DisplayControl_OnControlResized(object sender, RoutedEventArgs e)
        {
            Simulation.Scene.Camera.SetViewportValues(DisplayControl.AspectRatio, DisplayControl.ControlWidth, DisplayControl.ControlHeight);
            Simulation.Storage.SetDisplayDimensions(DisplayControl.ControlWidth, DisplayControl.ControlHeight, DisplayControl.AspectRatio);
            GL.Viewport(0, 0, DisplayControl.ControlWidth, DisplayControl.ControlHeight);
        }

        private void DisplayControl_OnControlUnloaded(object sender, RoutedEventArgs e)
        {
            foreach (Model item in Simulation.Scene.SceneObjects)
            {
                item.Dispose();
            }
            Simulation.Scene.SceneBackground?.Dispose();
        }

        private void DisplayControl_OnMouseDown(object sender, MouseButtonEventArgs e)
        {
            Simulation.Scene.Camera.SetLastMousePosition(DisplayControl.MousePosition);
            InputManager.OnMouseButtonChange(e.ChangedButton, true);
        }

        private void DisplayControl_OnMouseUp(object sender, MouseButtonEventArgs e)
        {
            InputManager.OnMouseButtonChange(e.ChangedButton, false);
        }

        private void DisplayControl_OnKeyDown(object sender, KeyEventArgs e)
        {
            InputManager.OnKeyChange(e.Key, true);
        }

        private void DisplayControl_OnKeyUp(object sender, KeyEventArgs e)
        {
            InputManager.OnKeyChange(e.Key, false);
        }

        private void DisplayControl_OnMouseWheel(object sender, MouseWheelEventArgs e)
        {
            InputManager.OnMouseScroll(e.Delta);
        }


    }
}
