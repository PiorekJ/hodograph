﻿#version 330 core

in layout(location = 0) vec3 position;
in layout(location = 1) vec3 normal;
in layout(location = 2) vec3 color;

out V_Out
{
	vec3 normal;
	vec3 fragPos;
	vec3 color;
} Out;

uniform mat4 model;
uniform mat4 view;
uniform mat4 projection;

mat4 projectionViewModel = projection*view*model;
void main()
{
	Out.fragPos = vec3(model * vec4(position, 1));
	Out.normal = mat3(transpose(inverse(model))) * normal;
	Out.color = color;
	gl_Position = projection * view * vec4(Out.fragPos,1);
}
