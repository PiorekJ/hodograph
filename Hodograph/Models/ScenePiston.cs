﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;
using Hodograph.Core;
using Hodograph.OpenTK;
using Hodograph.Utils;
using Hodograph.Utils.OpenTK;
using MathNet.Numerics.Distributions;
using OpenTK;

namespace Hodograph.Models
{
    public class ScenePiston : Model
    {
        private Mesh<VertexPC> _blockMesh;
        private Mesh<VertexPC> _wheelMesh;
        private Mesh<VertexPC> _connectorMesh;

        private Matrix4 _blockMatrix = Matrix4.Identity;
        private Matrix4 _wheelMatrix = Matrix4.Identity;
        private Matrix4 _connectorMatrix = Matrix4.Identity;
        
        public float AngularVelocity
        {
            get => _angularVelocity;
            set
            {
                _angularVelocity = value;
                RaisePropertyChanged();
            }
        }

        public float ConnectorLength
        {
            get => _connectorLength;
            set
            {
                _connectorLength = value;
                _wheelOrigin = Vector3.UnitX * (- _connectorLength);
                RaisePropertyChanged();
            }
        }

        public float WheelRadius
        {
            get => _wheelRadius;
            set
            {
                _wheelRadius = value;
                _wheelOrigin = Vector3.UnitX * (- _connectorLength);
                RaisePropertyChanged();
            }
        }

        public float Error
        {
            get => _error;
            set
            {
                _error = value;
                _normalDistGen = new Normal(0,_error);
                RaisePropertyChanged();
            }
        }

        private float _angularVelocity = 100.0f;
        private float _connectorLength = 10.0f;
        private float _wheelRadius = 4f;
        private float _error = 0.0f;
        private float _currentAngle = 0.0f;


        private Vector3 _wheelOrigin;
        private float _currentTime = 0.0f;
        private float _currentPosition = 0.0f;
        private float _previousPosition = 0.0f;

        private Normal _normalDistGen;

        public ScenePiston()
        {
            Shader = Shaders.BasicShader;
            _wheelMesh = MeshGenerator.GenerateHoledMarkedCircleMesh(0.15f, 1.0f, 0.1f, 64, 360, Colors.DarkGray, Colors.Lime, 6, 0.65f);
            _blockMesh = MeshGenerator.GenerateBoxMesh(5f, 2.5f, Colors.DarkGray, 2.5f);
            _connectorMesh = MeshGenerator.GenerateBoxMesh(1.0f, 0.2f, Colors.Lime, 0.5f);
            _wheelOrigin = Vector3.UnitX * (- ConnectorLength);
            _normalDistGen = new Normal(0,Error);
        }

        protected override void OnRender()
        {
            Shader.Use();
            Shader.Bind(Shader.GetUniformLocation("view"), Simulation.Scene.Camera.GetViewMatrix());
            Shader.Bind(Shader.GetUniformLocation("projection"), Simulation.Scene.Camera.GetProjectionMatrix());
            Shader.Bind(Shader.GetUniformLocation("model"), _wheelMatrix);
            _wheelMesh.Draw();
            Shader.Bind(Shader.GetUniformLocation("model"), _blockMatrix);
            _blockMesh.Draw();
            Shader.Bind(Shader.GetUniformLocation("model"), _connectorMatrix);
            _connectorMesh.Draw();
        }

        private int _chartCounter = 0;
        private bool _updateCharts = false;

        public void StartSimulation()
        {
            _currentTime = 0.0f;
            _currentPosition = WheelRadius;
            _previousPosition = WheelRadius;
            _chartCounter = 0;
        }

        protected override void OnUpdate()
        {
            if (Simulation.Settings.IsRunning)
            {
                var angleRadians = MathHelper.DegreesToRadians(-_currentAngle);
                var deviatedConnector = _connectorLength + (float)_normalDistGen.Sample();
                _wheelMatrix = Matrix4.CreateScale(WheelRadius) * Matrix4.CreateRotationZ(angleRadians) * Matrix4.CreateTranslation(_wheelOrigin);
                var pointVector = (Matrix3.CreateRotationZ(-angleRadians) * Vector3.UnitX * WheelRadius) + _wheelOrigin;
                var pistonPosition = WheelRadius * Math.Cos(angleRadians) + Math.Sqrt(
                                         deviatedConnector * deviatedConnector - WheelRadius * WheelRadius *
                                         Math.Sin(angleRadians) * Math.Sin(angleRadians));//pointVector.X + ConnectorLength;
                var pistonVectorPosition = _wheelOrigin + new Vector3((float)pistonPosition, 0, 0);
                var angleVector = pointVector - pistonVectorPosition;
                _connectorMatrix = Matrix4.CreateScale((float)deviatedConnector, 1, 1) * Matrix4.CreateRotationZ((float)Math.Atan2(-angleVector.Y, -angleVector.X)) * Matrix4.CreateTranslation(pointVector);
                _blockMatrix = Matrix4.CreateTranslation(pistonVectorPosition);
                var velocity = (pistonVectorPosition.X - _currentPosition) / 2 / Constants.SimulationDT;
                var acceleration = (pistonVectorPosition.X - 2 * _currentPosition + _previousPosition) / (Constants.SimulationDT * Constants.SimulationDT);
                if (_updateCharts)
                {
                    Simulation.Charts.AddToChart(Charts.ChartType.Position, _currentTime, pistonVectorPosition.X);
                    Simulation.Charts.AddToChart(Charts.ChartType.Velocity, _currentTime, velocity);
                    Simulation.Charts.AddToChart(Charts.ChartType.Acceleration, _currentTime, acceleration);
                    Simulation.Charts.AddToChart(Charts.ChartType.Phase, pistonVectorPosition.X, velocity);
                    _currentTime += Constants.SimulationDT;
                    _chartCounter = 0;
                }
                _previousPosition = _currentPosition;
                _currentPosition = pistonVectorPosition.X;
                _currentAngle += _angularVelocity * Constants.SimulationDT;
                _currentAngle %= 360;
                _chartCounter++;
                if (_chartCounter >= 2)
                    _updateCharts = true;
            }
        }

        public override void Dispose()
        {
            base.Dispose();
            _wheelMesh?.Dispose();
            _blockMesh?.Dispose();
            _connectorMesh?.Dispose();
        }
    }
}
