﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using Hodograph.Utils.Collections;
using OxyPlot;

namespace Hodograph.Core
{
    public class Charts: DependencyObject
    {
        public enum ChartType
        {
            Position,
            Velocity,
            Acceleration,
            Phase
        }

        public static readonly DependencyProperty PositionProperty = DependencyProperty.Register("Position", typeof(WpfObservableRangeCollection<DataPoint>), typeof(MainWindow), new PropertyMetadata(default(WpfObservableRangeCollection<DataPoint>)));

        public WpfObservableRangeCollection<DataPoint> Position
        {
            get => (WpfObservableRangeCollection<DataPoint>)GetValue(PositionProperty);
            set => SetValue(PositionProperty, value);
        }

        public static readonly DependencyProperty VelocityProperty = DependencyProperty.Register("Velocity", typeof(WpfObservableRangeCollection<DataPoint>), typeof(MainWindow), new PropertyMetadata(default(WpfObservableRangeCollection<DataPoint>)));

        public WpfObservableRangeCollection<DataPoint> Velocity
        {
            get => (WpfObservableRangeCollection<DataPoint>)GetValue(VelocityProperty);
            set => SetValue(VelocityProperty, value);
        }

        public static readonly DependencyProperty AccelerationProperty = DependencyProperty.Register("Acceleration", typeof(WpfObservableRangeCollection<DataPoint>), typeof(MainWindow), new PropertyMetadata(default(WpfObservableRangeCollection<DataPoint>)));

        public WpfObservableRangeCollection<DataPoint> Acceleration
        {
            get => (WpfObservableRangeCollection<DataPoint>)GetValue(AccelerationProperty);
            set => SetValue(AccelerationProperty, value);
        }

        public static readonly DependencyProperty PhaseProperty = DependencyProperty.Register("Phase", typeof(WpfObservableRangeCollection<DataPoint>), typeof(MainWindow), new PropertyMetadata(default(WpfObservableRangeCollection<DataPoint>)));

        public WpfObservableRangeCollection<DataPoint> Phase
        {
            get => (WpfObservableRangeCollection<DataPoint>)GetValue(PhaseProperty);
            set => SetValue(PhaseProperty, value);
        }


        public Charts()
        {
            Position = new WpfObservableRangeCollection<DataPoint>();
            Velocity = new WpfObservableRangeCollection<DataPoint>();
            Acceleration = new WpfObservableRangeCollection<DataPoint>();
            Phase = new WpfObservableRangeCollection<DataPoint>();
        }

        public void ClearCharts()
        {
            Position.Clear();
            Velocity.Clear();
            Acceleration.Clear();
            Phase.Clear();
        }

        public void AddToChart(ChartType type, float t, float value)
        {
            switch (type)
            {
                case ChartType.Position:
                    Position.Add(new DataPoint(t, value));
                    break;
                case ChartType.Velocity:
                    Velocity.Add(new DataPoint(t, value));
                    break;
                case ChartType.Acceleration:
                    Acceleration.Add(new DataPoint(t, value));
                    break;
                case ChartType.Phase:
                    Phase.Add(new DataPoint(t, value));
                    break;
            }

            if (Position.Count > Simulation.Settings.MaxChartPoints)
            {
                Position.RemoveRange(0, Position.Count - Simulation.Settings.MaxChartPoints);
            }
            if (Velocity.Count > Simulation.Settings.MaxChartPoints)
            {
                Velocity.RemoveRange(0, Velocity.Count - Simulation.Settings.MaxChartPoints);
            }
            if (Acceleration.Count > Simulation.Settings.MaxChartPoints)
            {
                Acceleration.RemoveRange(0, Acceleration.Count - Simulation.Settings.MaxChartPoints);
            }
            if (Phase.Count > Simulation.Settings.MaxChartPoints * 2)
            {
                Phase.RemoveRange(0, Phase.Count - Simulation.Settings.MaxChartPoints);
            }
        }
    }

}
