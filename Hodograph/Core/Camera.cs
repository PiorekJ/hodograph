﻿using Hodograph.Utils;
using Hodograph.Utils.Math;
using OpenTK;
using Key = System.Windows.Input.Key;
using MouseButton = System.Windows.Input.MouseButton;

namespace Hodograph.Core
{
    public class Camera : SceneObject
    {
        public float ZNear { get; set; } = Constants.DefaultZNear;
        public float ZFar { get; set; } = Constants.DefaultZFar;

        private Vector2 _lastMousePosition = Vector2.Zero;

        private float _zoomSize = 0.025f;

        public float OrthoX;
        public float OrthoY;

        private float _aspectRatio;
        public bool IsMouseMoving => InputManager.IsMouseButtonDown(MouseButton.Middle);
        public bool IsKeyboardMoving => InputManager.IsKeyDown(Key.W) || InputManager.IsKeyDown(Key.S) || InputManager.IsKeyDown(Key.A) || InputManager.IsKeyDown(Key.D) || InputManager.IsKeyDown(Key.Space) || InputManager.IsKeyDown(Key.LeftCtrl);

        public Matrix4 GetViewMatrix()
        {
            return MathGeneralUtils.CreateTranslation(-Transform.Position) * MathGeneralUtils.CreateFromQuaternion(Transform.Rotation.Inverted()); ;
        }

        public Matrix4 GetProjectionMatrix()
        {
            OrthoX = Simulation.Storage.DisplayWidth / 2.0f;
            OrthoY = Simulation.Storage.DisplayHeight / 2.0f;
            return Matrix4.CreateOrthographicOffCenter(-OrthoX * _zoomSize, OrthoX * _zoomSize, -OrthoY * _zoomSize, OrthoY * _zoomSize, ZNear, ZFar);
        }

        public void SetViewportValues(float aspectRatio, int width, int height)
        {
            _aspectRatio = aspectRatio;
        }

        public Camera()
        {
            Transform.Position = Constants.DefaultCameraPosition;
            InputManager.OnMouseScrollEvent += Zoom;
        }

        public void UpdateCamera(Vector2 mousePosition)
        {
            KeyboardControl();
            MouseControl(mousePosition);
        }

        private void MouseControl(Vector2 mousePosition)
        {
            if (_lastMousePosition == mousePosition)
                return;

            var diffX = mousePosition.X - _lastMousePosition.X;
            var diffY = mousePosition.Y - _lastMousePosition.Y;

            _lastMousePosition = mousePosition;

            if (IsMouseMoving)
            {
                Vector3 top = Vector3.Transform(-Vector3.UnitY, Transform.Rotation);
                Vector3 right = Vector3.Transform(Vector3.UnitX, Transform.Rotation);

                Transform.Position -= (top * diffY + right * diffX) * Constants.CameraMovementMouseSensitivity * Simulation.DeltaTime;
            }
        }

        private void Zoom(int delta)
        {
            if (delta < 0)
                _zoomSize += Constants.CameraZoomMouseSensitivity;
            //Transform.Position += Transform.Rotation * Vector3.UnitZ * Constants.CameraZoomMouseSensitivity;
            if (delta > 0 && _zoomSize > Constants.CameraMaxZoom)
                _zoomSize -= Constants.CameraZoomMouseSensitivity;
            //Transform.Position -= Transform.Rotation * Vector3.UnitZ * Constants.CameraZoomMouseSensitivity;
        }

        private void KeyboardControl()
        {
            float velocity = InputManager.IsKeyDown(Key.LeftShift) ? Constants.CameraMovementKeySlowVelocity : Constants.CameraMovementKeyVelocity;

            if (InputManager.IsKeyDown(Key.W))
            {
                Transform.Position += Transform.Rotation * Vector3.UnitY * velocity * Simulation.DeltaTime;
            }

            if (InputManager.IsKeyDown(Key.S))
            {
                Transform.Position -= Transform.Rotation * Vector3.UnitY * velocity * Simulation.DeltaTime;
            }

            if (InputManager.IsKeyDown(Key.A))
            {
                Transform.Position -= Transform.Rotation * Vector3.UnitX * velocity * Simulation.DeltaTime;
            }

            if (InputManager.IsKeyDown(Key.D))
            {
                Transform.Position += Transform.Rotation * Vector3.UnitX * velocity * Simulation.DeltaTime;
            }
        }

        public void SetLastMousePosition(Vector2 pos)
        {
            _lastMousePosition = pos;
        }

        protected override void OnRender() { }

        protected override void OnUpdate() { }
    }
}
