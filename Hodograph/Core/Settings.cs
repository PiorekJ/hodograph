﻿using Hodograph.Utils.Math;
using Hodograph.Utils.UIExtensionClasses;
using OpenTK;

namespace Hodograph.Core
{
    public class Settings : BindableObject
    {
        private bool _isRunning;
        private bool _uiEnabled = true;
        private int _maxChartPoints = 250;

        public bool UIEnabled
        {
            get { return _uiEnabled; }
            set
            {
                _uiEnabled = value;
                RaisePropertyChanged();
            }
        }

        public bool IsRunning
        {
            get { return _isRunning; }
            set
            {
                _isRunning = value;
                RaisePropertyChanged();
            }
        }

        public int MaxChartPoints
        {
            get { return _maxChartPoints; }
            set
            {
                _maxChartPoints = value;
                RaisePropertyChanged();
            }
        }
       

    }
}
