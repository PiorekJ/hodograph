﻿using System.Collections.ObjectModel;
using System.Windows.Input;
using System.Windows.Media;
using Hodograph.Models;
using Hodograph.Utils.UIExtensionClasses;
using OpenTK;

namespace Hodograph.Core
{
    public class Scene : BindableObject
    {
        private ScenePiston _scenePiston;
        public Camera Camera { get; set; }

        public SceneBackground SceneBackground { get; set; }

        public ScenePiston ScenePiston
        {
            get => _scenePiston;
            set
            {
                _scenePiston = value;
                RaisePropertyChanged();
            }
        }

        public ScenePoint DebugPoint { get; set; }

        public ObservableCollection<Model> SceneObjects { get; set; }

        
        public Scene()
        {
            Camera = new Camera();
            SceneObjects = new ObservableCollection<Model>();
        }
        
        public SceneBackground AddSceneBackground()
        {
            SceneBackground = new SceneBackground();
            return SceneBackground;
        }

        public ScenePiston AddScenePiston()
        {
            ScenePiston = new ScenePiston();
            SceneObjects.Add(ScenePiston);
            return ScenePiston;
        }

        public ScenePoint AddScenePoint()
        {
            DebugPoint = new ScenePoint();
            DebugPoint.Color = Colors.Red;
            DebugPoint.Transform.Position = Vector3.Zero;
            SceneObjects.Add(DebugPoint);
            return DebugPoint;
        }
    }
}