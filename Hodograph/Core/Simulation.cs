﻿using System.Windows.Input;
using Hodograph.Utils;
using Hodograph.Utils.UIExtensionClasses;

namespace Hodograph.Core
{
    public class Simulation
    {
        public static Scene Scene { get; set; }
        public static Settings Settings { get; set; }
        public static Storage Storage { get; set; }
        public static Charts Charts { get; set; }

        public TimeCounter TimeCounter;

        public static float DeltaTime => TimeCounter.DeltaTime;

        #region Commands

        public ICommand StartCommand { get; set; }
        public ICommand PauseCommand { get; set; }
        public ICommand StopCommand { get; set; }

        #endregion

        public Simulation()
        {
            Scene = new Scene();
            Settings = new Settings();
            TimeCounter = new TimeCounter();
            Storage = new Storage();
            Charts= new Charts();
            StartCommand = new RelayCommand(param => StartSimulation());
            PauseCommand = new RelayCommand(param => PauseSimulation());
            StopCommand = new RelayCommand(param => StopSimulation());
        }

        private void StartSimulation()
        {
            Settings.UIEnabled = false;
            Settings.IsRunning = true;
            Scene.ScenePiston.StartSimulation();
        }

        private void PauseSimulation()
        {
            Settings.IsRunning = false;
        }

        private void StopSimulation()
        {
            Settings.UIEnabled = true;
            Settings.IsRunning = false;
        }

        public void InitializeSimulation()
        {
            Settings.IsRunning = true;
            TimeCounter.Start();
            Scene.AddSceneBackground();
            Scene.AddScenePiston();
            Scene.AddScenePoint();
            Scene.ScenePiston.StartSimulation();
        }
    }
}
