﻿using Hodograph.Utils.UIExtensionClasses;

namespace Hodograph.Core
{
    public abstract class Component : BindableObject
    {
        public SceneObject Owner;

        protected Component()
        {
        }
    }
}
