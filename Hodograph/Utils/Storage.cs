﻿using Hodograph.Utils.UIExtensionClasses;

namespace Hodograph.Utils
{
    public class Storage : BindableObject
    {
        public int DisplayWidth;
        public int DisplayHeight;
        public float DisplayAspect;

        public void SetDisplayDimensions(int displayWidth, int displayHeight, float displayAspect)
        {
            DisplayWidth = displayWidth;
            DisplayHeight = displayHeight;
            DisplayAspect = displayAspect;
        }
    }
}
