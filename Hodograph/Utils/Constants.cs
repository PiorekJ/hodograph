﻿using System.Windows.Media;
using OpenTK;

namespace Hodograph.Utils
{
    public static class Constants
    {
        public const int LineSize = 2;

        //public static Vector3 DefaultCameraPosition = new Vector3(0, 0, 3);
        //public const float DefaultFOV = (float)System.Math.PI / 4;
        //public const float DefaultZNear = 0.00001f;
        //public const float DefaultZFar = 500f;

        public static Vector3 DefaultCameraPosition = new Vector3(0, 0, 0);
        public const float DefaultFOV = (float)System.Math.PI / 4;
        public const float DefaultZNear = 1f;
        public const float DefaultZFar = -1f;
        public const float CameraMaxZoom = 0.001f;

        //public const float CameraMovementMouseSensitivity = 30.0f;
        //public const float CameraRotationMouseSensitivity = 0.05f;
        //public const float CameraZoomMouseSensitivity = 0.05f;

        //public const float CameraMovementKeyVelocity = 300.0f;
        //public const float CameraMovementKeySlowVelocity = 150.0f;

        public const float CameraMovementMouseSensitivity = 0.5f;
        public const float CameraRotationMouseSensitivity = 0.05f;
        public const float CameraZoomMouseSensitivity = 0.001f;

        public const float CameraMovementKeyVelocity = 5.0f;
        public const float CameraMovementKeySlowVelocity = 0.25f;


        public const float CursorMovementKeySlowVelocity = 0.5f;
        public const float CursorRotationKeySlowVelocity = 20.0f;

        public static readonly Color BackgroundColor = Colors.White;
        public static readonly Color BackgroundStripeColor = Colors.GhostWhite;

        public static readonly float QuaternionThreshold = 0.9995f;
        public static float Epsilon = 0.000001f;

        public static float SimulationDT = 1 / 30.0f;
    }
}
